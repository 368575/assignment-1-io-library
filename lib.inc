section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov  rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov  rax, rdi
.repeat:
    cmp  byte [rax], 0
    jz   .done
    inc  rax
    jmp  .repeat
.done:
    sub  rax, rdi
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    mov  rdx, rax
    pop  rsi
    mov  rdi, 1
    mov  rax, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov  rdi, 1
    mov  rsi, rsp
    mov  rdx, 1
    mov  rax, 1
    syscall
    pop  rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov  rdi, 0xA
    jmp  print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rbx
    push rcx
    mov  rax, rdi
    mov  rbx, 10
    mov  rcx, 23
    sub  rsp, 24
    mov  byte [rsp + rcx], 0
.to_chars:
    xor  rdx, rdx
    div  rbx
    dec  rcx
    add  dl, 0x30
    mov  byte [rsp + rcx], dl
    test rax, rax
    jnz  .to_chars
    lea  rdi, byte [rsp + rcx]
    call print_string
    add  rsp, 24
    pop  rcx
    pop  rbx
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns  .print
    push rdi
    mov  rdi, '-'
    call print_char
    pop  rdi
    neg  rdi
.print:
    jmp  print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push rbx
    push rcx
    xor  rcx, rcx
.repeat:
    mov  al, byte [rdi + rcx]
    mov  bl, byte [rsi + rcx]
    cmp  al, bl
    jne  .not_equal
    inc  rcx
    test al, al
    jnz  .repeat
    jmp  .equal
.not_equal:
    xor  rax, rax
    jmp  .exit
.equal:
    mov  rax, 1
.exit:
    pop  rcx
    pop  rbx
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    sub  rsp, 8
    xor  rax, rax
    xor  rdi, rdi
    mov  rsi, rsp
    mov  rdx, 1
    syscall
    test rax, rax
    jz   .read_eof
    mov  rax, [rsp]
.read_eof:
    add  rsp, 8
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r12
    push r13
    push rbx
    mov  rbx, rdi
    mov  r12, rsi
    xor  r13, r13
.read:
    call read_char
    cmp  rax, 0x9
    je   .test_skip_ws
    cmp  rax, 0xA
    je   .test_skip_ws
    cmp  rax, 0x20
    je   .test_skip_ws
    test rax, rax
    jz   .test_read_fail
    cmp  r12, r13
    jle  .read_fail
    mov  byte [rbx + r13], al
    inc  r13
    jmp  .read
.test_skip_ws:
    test r13, r13
    jz   .read
    jmp  .read_ok
.test_read_fail:
    test r13, r13
    jnz  .read_ok
.read_fail:
    xor  rax, rax
    xor  rdx, rdx
    jmp  .finish
.read_ok:
    mov  byte [rbx + r13], 0
    mov  rax, rbx
    mov  rdx, r13
.finish:
    pop  rbx
    pop  r13
    pop  r12
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    push rbx
    xor  rax, rax
    xor  rbx, rbx
    xor  rdx, rdx
.next_char:
    mov  bl, byte [rdi + rdx]
    test bl, bl
    jz   .finish
    cmp  rbx, '0'
    jl   .finish
    cmp  rbx, '9'
    jg   .finish
    sub  rbx, '0'
    push rdx
    mov  rdx, 10
    mul  rdx
    add  rax, rbx
    pop  rdx
    inc  rdx
    jmp  .next_char
.finish:
    pop  rbx
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    push rbx
    mov  bl, byte [rdi]
    cmp  bl, '-'
    je   .skip_sign
    cmp  bl, '+'
    jne  .parse
.skip_sign:
    mov  rdx, rdi
    lea  rdi, byte [rdx + 1]
.parse:
    call parse_uint
    cmp  bl, '-'
    je   .minus
    cmp  bl, '+'
    jne  .finish
    jmp  .has_sign
.minus:
    neg  rax
.has_sign:
    inc  rdx
.finish:
    pop  rbx
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop  rdx
    pop  rsi
    pop  rdi
    cmp  rdx, rax
    jle  .error
    push rbx
    push rcx
    mov  rcx, rax
    inc  rcx
.loop:
    dec  rcx
    mov  bl, byte [rdi + rcx]
    mov  byte [rsi + rcx], bl
    test rcx, rcx
    jnz  .loop
    pop  rcx
    pop  rbx
    jmp  .finish
.error:
    xor  rax, rax
.finish:
    ret
